import { createElement } from "../function/dom.js"
import { cloneTemplate } from "../function/dom.js"

/**
 * @typedef {object} Todo
 * @property {number} id
 * @property {string} title
 * @property {boolean} completed
 */
export class TodoList{
    /**@type {Todo[]} */
    #todos = []
    /**@type {HTMLElement[]} */
    #listElement = []
    /**
     * 
     * @param {Todo[]} todos 
     */
    constructor(todos){
        this.#todos = todos
    }
    /**
     * 
     * @param {HTMLElement} element 
     */
    appendTo(element){
        element.append(
            cloneTemplate("todolist-layout")
        )
        
        this.#listElement  = element.querySelector(".list-group")
        
        for (let todo of this.#todos){
            const t = new TodoIteme(todo)
            this.#listElement.prepend(t.element)
            
        }
        element.querySelector('form').addEventListener("submit",(e)=>this.#onSubmit(e))
        element.querySelectorAll(".btn-group button").forEach(button=>{
            button.addEventListener('click',e=>this.#toggleFilter(e))
        })
        this.#listElement.addEventListener("delete",({detail:todo})=>{
            this.#todos = this.#todos.filter(t=> t!==todo)
            this.#onUppdate()
        })
        this.#listElement.addEventListener("toggle",({detail:todo})=>{
            todo.completed = !todo.completed
            this.#onUppdate()
        })

        

    }
    /**
     * 
     * @param {SubmitEvent} e 
     */
    #onSubmit(e){
        e.preventDefault()
        const title = new FormData(e.currentTarget).get('title').toString().trim()
        if(title===""){
            return
        }
        const todo = {
            id: Date.now(),
            title,
            completed : false
        }
        const item = new TodoIteme(todo)
       this.#listElement.prepend(item.element)
       this.#todos.push(todo)
       this.#onUppdate()
       e.currentTarget.reset()

    }

    #onUppdate(){
        localStorage.setItem('todos',JSON.stringify(this.#todos))
    }
    /**
     * 
     * @param {PointerEvent} e 
     */
    #toggleFilter(e){
        e.preventDefault()
        const filter = e.currentTarget.getAttribute('data-filter')
        e.currentTarget.parentElement.querySelector('.active').classList.remove('active')
        e.currentTarget.classList.add("active")
        
        if(filter==="done"){
            this.#listElement.classList.remove('hide-completed')
            this.#listElement.classList.add('hide-todo')
        }else if (filter==="todo"){
            this.#listElement.classList.add('hide-completed')
            this.#listElement.classList.remove('hide-todo')
            
        }else{
            this.#listElement.classList.remove('hide-completed')
            this.#listElement.classList.remove('hide-todo')
        }
    }
}


class TodoIteme{

    #element
    #todo
    /**@type {Todo}*/
    constructor(todo){
        this.#todo = todo
        const li= cloneTemplate('todolist-item').firstElementChild
        
        this.#element = li
        const checkbox = li.querySelector("input")
        checkbox.setAttribute("id",`todo-${todo.id}`)
        if (todo.completed){
            checkbox.setAttribute("checked",'')
        }
        const label = li.querySelector('label')
        label.setAttribute('for',`todo-${todo.id}`)
        label.innerText= todo.title
        const button = li.querySelector('button')
        this.toggle(checkbox)
        button.addEventListener("click",(e)=>{
           this.remove(e) 
        })
        checkbox.addEventListener("change", e =>this.toggle(e.currentTarget))
        
        

    }
    /**
     * @param {HTMLElement} element
     */
    get element(){
        return this.#element
    }
    /**
     * 
     * @param {PointeEvent} e 
     */
    remove(e){
        e.preventDefault()
        const event = new CustomEvent('delete',{
            detail : this.#todo,
            bubbles: true,
            cancelable: true
        })
        this.#element.dispatchEvent(event)
        if (event.defaultPrevented){
            return
        }
        this.#element.remove()
    }
    /**
     * change etat (a faire/faire)de la tache
     * @param {HMLInputElemnt} checkbox 
     */

    toggle(checkbox){
        if( checkbox.checked){
            this.#element.classList.add('is-completed')

        }else{
            this.#element.classList.remove('is-completed')
        }
        const event = new CustomEvent('toggle',{
            detail : this.#todo,
            bubbles: true,
            
        })
        this.#element.dispatchEvent(event)
        
       
    }
}