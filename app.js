// const buttons = document.querySelectorAll("button")
// const todoListe = document.querySelector("form input")
// const ajoutTache = document.querySelector(".list-group")
// const div = document.createElement("li")
// const lis = document.querySelectorAll('ul li')
// const trashs = document.querySelectorAll(".bi-trash")

// // todoListe.addEventListener("input",(e)=>{
// //     if(e.target.value)
// // })
// buttons.forEach(button=>{
//         button.addEventListener("click", e=>{
//             e.preventDefault()
//             const checkeds = document.querySelectorAll('ul input')
//             const nbLis= lis.length

//             if(button.innerText=="Ajouter") {
//                 if(todoListe.value!=""){
//                div.innerHTML=(`
//                <input class="form-check-input" type="checkbox" id="todo-${nbLis+1}">
//                <label class="ms-2 form-check-label" for="todo-${nbLis+1}">
//                    ${todoListe.value}
//                </label>
//                <label class="ms-auto btn btn-danger btn-sm">
//                <i class="bi-trash">
//                </i>
//                </label>
//             `)
//             div.classList.add("todo", "list-group-item", "d-flex", "align-items-center")
//                ajoutTache.append(div)
               
//                 }
                
//                }
//            if(button.innerText=="A faire") {
//             checkeds.forEach(checked=>{
//                 checked.parentNode.classList.remove('d-none')
//                 if (checked.checked==true){
//                     checked.parentNode.classList.add('d-none')
//                 }
//             })
//            }
//            if(button.innerText=="Toutes") {
//             checkeds.forEach(checked=>{
//                 checked.parentNode.classList.remove('d-none')
//             })
//            }
//            if(button.innerText=="Faites") {
            
//             checkeds.forEach(checked=>{
//                 checked.parentNode.classList.remove('d-none')
//                 if (checked.checked!=true){
//                     checked.parentNode.classList.add('d-none')
//                 }
//             })
//            }
           
//         } )
        
//     })

// trashs.forEach(trash=>{
//     trash.addEventListener("click", e=>{
//         trash.parentNode.parentNode.remove()
//     })
// })

import { fetchJson } from "./function/api.js";
import {createElement} from "./function/dom.js";
import { TodoList } from "./components/Todoliste.js";

try{
    const todos =await fetchJson("https://jsonplaceholder.typicode.com/todos?_limit=5")
    // let todos = []
    // const todosInStorage = localStorage.getItem("todos")?.toString()
    // if(todosInStorage){
    //     todos = JSON.parse(todosInStorage)
    // }
    const list = new TodoList(todos)
    list.appendTo(document.querySelector("#todolist"))
}catch(e){
    const alertElement = createElement('div',{
        class : "alert alert-danger m-2",
        role : "alert"

    })
    alertElement.innerText = "Impossible de recuperer les liste"
    document.body.prepend(alertElement)
    console.error(e)
}

